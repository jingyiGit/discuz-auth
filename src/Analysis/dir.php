<?php

namespace DiscuzAuth\Analysis;

trait dir
{
  /**
   * 递归扫描文件夹
   *
   * @param string $dir
   * @return array
   */
  private function scanDirFile($dir)
  {
    $result = [];
    $dir    = $this->finish($dir, '/');
    $cdir   = scandir($dir);
    foreach ($cdir as $value) {
      if (in_array($value, [".", ".."])) {
        continue;
      }
      
      if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
        $result[$value] = $this->scanDirFile($dir . $value);
      } else {
        // 只匹配PHP文件
        if (preg_match('/^(\w+)\.php$/', $value)) {
          $result[]         = $value;
          $this->fileList[] = $dir . $value;
        }
      }
    }
    return $result;
  }
  
  /**
   * 删除文件夹及其文件夹下所有文件
   *
   * @param string $dir 目标目录
   * @return bool
   */
  private function deldir($dir)
  {
    //先删除目录下的文件：
    $dh = opendir($dir);
    while ($file = readdir($dh)) {
      if ($file != "." && $file != "..") {
        $fullpath = $dir . "/" . $file;
        if (!is_dir($fullpath)) {
          unlink($fullpath);
        } else {
          $this->deldir($fullpath);
        }
      }
    }
    
    closedir($dh);
    //删除当前文件夹：
    if (rmdir($dir)) {
      return true;
    } else {
      return false;
    }
  }
}
