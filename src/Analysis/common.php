<?php

namespace DiscuzAuth\Analysis;

trait common
{
  /**
   * 给字符开始位置加前缀
   *
   * @param string $value
   * @param string $prefix
   * @return string
   */
  private function start($value, $prefix)
  {
    $quoted = preg_quote($prefix, '/');
    
    return $prefix . preg_replace('/^(?:' . $quoted . ')+/u', '', $value);
  }
  
  /**
   * 给字符开始位置加后缀
   *
   * @param string $value
   * @param string $cap
   * @return string
   */
  private function finish($value, $cap)
  {
    $quoted = preg_quote($cap, '/');
    return preg_replace('/(?:' . $quoted . ')+$/u', '', $value) . $cap;
  }
}
