<?php

namespace DiscuzAuth;

use DiscuzAuth\Analysis\common;
use DiscuzAuth\Analysis\dir;
use ReflectionClass;
use ReflectionMethod;

class Analysis
{
  use common;
  use dir;
  
  protected array $config = [];
  private string $tempDir = __DIR__ . '/temp/';
  private array $fileList = [];     // 首次扫描的文件列表
  private array $waitList = [];     // 待导出的列表
  private array $result = [];       // 最终返回的结果
  
  function __construct($ROOT_PATH, $config = [])
  {
    // 网站根目录(www)
    if (!defined('ROOT_PATH')) {
      define('ROOT_PATH', $ROOT_PATH);
    }
    
    // 兼容各种情况
    foreach ($config as &$v) {
      $v['namespace'] = rtrim($this->start($v['namespace'], '\\'), '\\');
      $v['path']      = ltrim($this->finish($v['path'], '/'), '/');
    }
    $this->config = $config;
    
    // 创建临时目录
    mkdir($this->tempDir);
    
    // 删除临时目录
    register_shutdown_function(function () {
      $this->deldir($this->tempDir);
    });
  }
  
  /**
   * 开始导出
   *
   * @return array
   * @throws \ReflectionException
   */
  public function export()
  {
    // 第1步，同步文件
    foreach ($this->config as $scanTarget) {
      if (!is_dir($scanTarget['path'])) {
        continue;
      }
      
      // 扫描述文件，扫描后存放在 $this->fileList
      $this->fileList = [];
      $this->scanDirFile($scanTarget['path']);
      
      // 同步文件（将文件同步到临时目录）
      $this->syncFile($this->fileList, $scanTarget);
    }
    
    // 第2步，开始导出
    $result = [];
    foreach ($this->waitList as $v) {
      if ($temp = $this->exportRule([
        'className' => $v['className'],
        'namespace' => $v['namespace'],
      ])) {
        $result[$v['scanTarget']['name']][] = $temp;
      }
    }
    
    // 导出
    $temp = [];
    foreach ($this->config as $config) {
      $temp[$config['name']] = $this->create($result[$config['name']]);
    }
    return $temp;
  }
  
  /**
   * 同步文件（将文件同步到临时目录）
   *
   * @param array $fileList
   * @param array $scanTarget
   * @return void
   */
  private function syncFile($fileList, $scanTarget)
  {
    // 创建目标目录
    $targetDir = $this->finish($this->tempDir . basename($scanTarget['path']), '/');
    if (!is_dir($targetDir)) {
      if (!@mkdir($targetDir)) {
        exit('创建目录失败, 请将[' . dirname(__DIR__) . ']目录改成www，并有读写权限');
      }
    }
    
    foreach ($fileList as $file) {
      // 如：adminList.php | test/aaa.php
      $tempFile = str_replace($scanTarget['path'], '', $file);
      
      // 创建子目录
      $dir = dirname($targetDir . $tempFile);
      if (!is_dir($dir)) {
        if (!mkdir($dir)) {
          @mkdir(dirname($dir));
          @mkdir($dir);
        }
      }
      
      // 处理命名空间，并同步文件
      $this->handleAndSyncFile(ROOT_PATH . $file, $targetDir . $tempFile, $scanTarget);
    }
  }
  
  private function handleAndSyncFile($fromFile, $toFile, $scanTarget)
  {
    // 新的命名空间
    $namespace = __NAMESPACE__ . "\\temp\\" . basename($scanTarget['path']);
    
    // + 子目录的命名空间
    $temp = str_replace(ROOT_PATH . $scanTarget['path'], '', $fromFile);
    if (strpos($temp, '/') && $sub_namespace = dirname($temp)) {
      $namespace .= '\\' . $sub_namespace;
    }
    
    // 不包含后缀的类名(文件名与类名必须相同)
    $className = basename($fromFile, '.php');
    
    // 替换类名
    $classContent = file_get_contents($fromFile);
    
    /// 替换命名空间
    $classContent = preg_replace("/namespace\s(.*);/", 'namespace ' . $namespace . ";", $classContent);
    
    // 替换use命名空间
    $oldNamespace = trim($scanTarget['namespace'], '\\');
    $oldNamespace = str_replace('\\', '.', $oldNamespace);
    $reg          = '#use.+' . $oldNamespace . '#mi';
    $classContent = preg_replace($reg, 'use ' . $namespace, $classContent);
    
    // 写出
    file_put_contents($toFile, $classContent);
    
    $this->waitList[] = [
      'className'  => $className,
      'namespace'  => $namespace,
      'scanTarget' => $scanTarget,
    ];
  }
  
  /**
   * 开始导出并分析
   *
   * @param $param
   * @return array|false
   * @throws \ReflectionException
   */
  private function exportRule($param)
  {
    if (!class_exists($param['namespace'] . '\\' . $param['className'])) {
      return false;
    }
    
    // 反射机制调用类的注释和方法名
    $reflector = new ReflectionClass($param['namespace'] . '\\' . $param['className']);
    
    // 取类的备注
    $classComment = $reflector->getDocComment();
    
    // 忽略的类
    if (stripos($classComment, "@internal") !== false) {
      return false;
    }
    
    // 取注释中的icon和备注
    preg_match_all('#(@.*?)\n#s', $classComment, $annotations);
    $controllerIcon   = 'layui-icon layui-icon-app';
    $controllerRemark = '';
    $controllerGroup  = [];
    if (isset($annotations[1])) {
      foreach ($annotations[1] as $tag) {
        if (stripos($tag, '@icon') !== false) {
          $controllerIcon = trim(substr($tag, stripos($tag, ' ') + 1));
        }
        if (stripos($tag, '@remark') !== false) {
          $controllerRemark = substr($tag, stripos($tag, ' ') + 1);
        }
        if (stripos($tag, '@group') !== false) {
          $temp = [];
          [$temp['name'], $temp['title']] = explode('.', substr($tag, stripos($tag, ' ') + 1), 2);
          $temp['name']    = trim($temp['name']);
          $temp['title']   = trim($temp['title']);
          $controllerGroup = $temp;
        }
      }
    }
    
    // 类标题，过滤掉其它字符
    $controllerTitle = trim(preg_replace([
      '/^\/\*\*(.*)[\n\r\t]/u',
      '/[\s]+\*\//u',
      '/\*\s@(.*)/u',
      '/[\s|\*]+/u',
    ], '', $classComment));
    $classList       = [
      'title'   => $controllerTitle,
      'name'    => $param['className'],
      'icon'    => $controllerIcon,
      'remark'  => $controllerRemark,
      'group'   => $controllerGroup,
      'methods' => [],
    ];
    
    // 只匹配公共的方法
    $methods = $reflector->getMethods(ReflectionMethod::IS_PUBLIC);
    foreach ($methods as $n) {
      // 过滤特殊的类
      if (substr($n->name, 0, 2) == '__' || $n->name == '_initialize') {
        continue;
      }
      
      // 取方法备注
      $comment = $reflector->getMethod($n->name)->getDocComment();
      
      // 忽略的方法
      if (stripos($comment, "@internal") !== false) {
        continue;
      }
      
      // 取方法标题，过滤掉其它字符
      $title = preg_replace([
        '/^\/\*\*(.*)[\n\r\t]/u',
        '/[\s]+\*\//u',
        '/\*\s@(.*)/u',
        '/[\s|\*]+/u',
      ], '', $comment);
      $title = $title ?: $n->name;
      
      $classList['methods'][] = array_merge([
        'title' => $title,
        'name'  => $n->name,
      ], $this->getMethodInfo($comment));// 取方法信息
    }
    
    return $classList;
  }
  
  /**
   * 取方法信息
   *
   * @param string $comment 方法备注
   * @return array
   */
  private function getMethodInfo($comment)
  {
    $handleParam = function ($value) {
      $value = str_replace(['   ', '  '], ' ', $value);
      $value = str_replace(['   ', '  '], ' ', $value);
      $temp  = array_filter(explode(' ', $value, 3));
      foreach ($temp as &$v) {
        $v = trim($v);
      }
      if (count($temp) == 3) {
        return [
          'type'   => $temp[0],
          'name'   => str_replace('$', '', $temp[1]),
          'remark' => $temp[2],
        ];
      } elseif (count($temp) == 2) {
        if (strpos($temp[0], '$') !== false) {
          return [
            'type'   => null,
            'name'   => str_replace('$', '', $temp[0]),
            'remark' => $temp[1],
          ];
        } else {
          return [
            'type'   => $temp[0],
            'name'   => str_replace('$', '', $temp[1]),
            'remark' => '',
          ];
        }
      } else {
        if (strpos($temp[0], '$') !== false) {
          return [
            'type'   => null,
            'name'   => str_replace('$', '', $temp[0]),
            'remark' => '',
          ];
        } else {
          return [];
        }
      }
      return $temp;
    };
    
    preg_match_all('#(@.*?)\n#s', $comment, $res);
    $info = [];
    if (isset($res[1])) {
      foreach ($res[1] as $tag) {
        if (stripos($tag, '@return') !== false) {
          $info['return'] = trim(substr($tag, stripos($tag, ' ') + 1));
          
        } else if (stripos($tag, '@remark') !== false) {
          $info['remark'] = substr($tag, stripos($tag, ' ') + 1);
          
        } else if (stripos($tag, '@param') !== false && strlen(trim($tag)) > 6) {
          if ($temp = $handleParam(trim(substr($tag, stripos($tag, ' ') + 1)))) {
            $info['params'][] = $temp;
          }
        }
      }
    }
    return $info;
  }
  
  private function create($auth)
  {
    $list = [];
    foreach ($auth as $class) {
      $children = [];
      $group    = '';
      if (isset($class['group']) && $class['group']['name']) {
        $group = $class['group']['name'];
        if (!isset($list[$group])) {
          $list[$group] = [
            'id'       => 'group.' . $group,
            'mark'     => 'group.' . $group,
            'title'    => $class['group']['title'] ?: $group,
            'spread'   => 1,
            'children' => [],
          ];
        }
      }
      
      foreach ($class['methods'] as $method) {
        $children[] = [
          'id'     => $class['name'] . '.' . $method['name'],
          'title'  => $method['title'],
          'mark'   => $method['name'],
          'spread' => 1,
        ];
      }
      
      $tempV = [
        'id'       => $class['name'],
        'title'    => $class['title'],
        'mark'     => $class['name'],
        'spread'   => 1,
        'children' => $children,
      ];
      
      // 是否分组
      if ($group) {
        $list[$group]['children'][] = $tempV;
      } else {
        $list[] = $tempV;
      }
    }
    sort($list);
    return $list;
  }
}
